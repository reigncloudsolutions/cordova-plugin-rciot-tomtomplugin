var exec = require('cordova/exec');
var TomTomPlugin = {
    openNavAppClient: function(lat, lng, fnSuccess, fnError) {
        exec(fnSuccess, fnError, 'TomTomPlugin', 'openNavAppClient', 
			[{
                "longitude": lng,
                "latitude": lat
            }]
		);
    },
	
	createTrip: function(waypoints, fnSuccess, fnError) {
        //noinspection JSAnnotator
        exec(fnSuccess, fnError, 'TomTomPlugin', 'createTrip', [{waypoints}]);
    },

    cancelTrip: function(fnSuccess, fnError) {
        exec(fnSuccess, fnError, 'TomTomPlugin', 'cancelTrip', []);
    },

    launch: function(fnSuccess, fnError) {
        exec(fnSuccess, fnError, 'TomTomPlugin', 'launch', []);
    },
	
    startup: function(fnSuccess, fnError) {
        exec(fnSuccess, fnError, 'TomTomPlugin', 'startup', []);
    }	
};
module.exports = TomTomPlugin;