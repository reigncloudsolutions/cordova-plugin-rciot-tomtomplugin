package com.rciot.tomtomplugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import com.tomtom.navapp.Address;
import com.tomtom.navapp.Build;
import com.tomtom.navapp.GeoCoder;
import com.tomtom.navapp.LocationManager;
import com.tomtom.navapp.MapInfo;
import com.tomtom.navapp.NavAppClient;
import com.tomtom.navapp.Routeable;
import com.tomtom.navapp.RouteableInfo;
import com.tomtom.navapp.Trip;
import com.tomtom.navapp.TripEvent;
import com.tomtom.navapp.TripEvent.ModifyResult;
import com.tomtom.navapp.TripEventManager;
import com.tomtom.navapp.TripManager;
import com.tomtom.navapp.Utils;
import com.tomtom.navapp.LocationManager.DisplayLocationListener;
import com.tomtom.navapp.LocationManager.DisplayLocationResult;
import com.tomtom.navapp.Trip.PlanResult;
import com.tomtom.navapp.Trip.PlanListener;
import com.tomtom.navapp.Trip.RequestError;
//import com.tomtom.navapp.Trip.SpeedLimitResult;
//import com.tomtom.navapp.Trip.SpeedShieldShape;
//import com.tomtom.navapp.Trip.SpeedShieldLuminance;
import com.tomtom.navapp.ErrorCallback;
import com.tomtom.navapp.internals.NavAppClientUtils;
import com.tomtom.navapp.Debug;
import android.content.Context;
import android.content.res.Resources;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;

import android.app.Activity;
import android.widget.Toast;

import com.tomtom.navapp.NavAppError;

public class TomTomPlugin extends CordovaPlugin {
	public static final String ACTION_OPEN_NAV_APP_ENTRY = "openNavAppClient"; 
	private static final String ATTR_ADR_HOUSE_NUMBER   = "house_number";
    private static final String ATTR_ADR_STREET         = "street";
    private static final String ATTR_ADR_CITY           = "city";
    private static final String ATTR_ADR_POSTAL_CODE    = "postal_code";
    private static final String ATTR_ADR_COUNTRY        = "country";
    private static final String ATTR_ADR_COUNTRY_CODE   = "country_code";
    private static final String ATTR_ADR_PROVINCE       = "province";
    private static final String ATTR_ADR_STATE          = "state";
    private static final String ATTR_ADR_STATE_CODE     = "state_code";
    private static final String ATTR_ADR_COUNTY         = "county";
    private static final String ATTR_ADR_DISTRICT       = "district";
    private static final String ATTR_ADR_LOCALITY       = "locality";
    private static final String ATTR_ADR_BUILDING_NAME  = "building_name";
    private static final String ATTR_ADR_APARTMENT_NAME = "apartment_name";


	private NavAppClient mNavappClient = null;
	private final static String TAG = "TomTomPlugin";
    private TripManager mTripManager = null;
    private Trip mOldTrip = null;
    private TripEventManager mTripEventManager = null;
	private Context context;
	private Trip mTrip = null;
	private boolean mTripListenerRegistered = false;
	private boolean mTripProgressListenerRegistered = false;
	private boolean mTripWaypointsEtaChangeListener = false;
	private boolean mWaypointsArrivalListenerRegistered = false;
	
	private Debug mDebug = null;
	final Intent intent = new Intent("didShow");

	Bundle b = new Bundle();
	
	private NavAppClient getClient() {
		return mNavappClient;
    }

	private ErrorCallback mErrorCallback = new ErrorCallback() {
		@Override
		public void onError(NavAppError error) {
			Log.e(TAG, "onError(" + error.getErrorMessage() + ")\n" + error.getStackTraceString());
		}
	};

	public boolean createNavAppClient() {
		if (mNavappClient == null) {
			// Create the NavAppClient
			try {
				mNavappClient = NavAppClient.Factory.make(this.cordova.getActivity(), mErrorCallback);
			} catch (RuntimeException e) {
				Log.e(TAG, "Failed creating NavAppClient", e);
				return false;
			}
		}
		initInterfaces();
		return true;
	}

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		createNavAppClient();
		context = this.cordova.getActivity();
		//Toast.makeText(context, "debug line 82 action = " + action, Toast.LENGTH_SHORT).show();
		if (action.equals("startup")){
			startup(callbackContext);
			return true;
		} else if (action.equals("openNavAppClient")) {
			try{
				planTrip(callbackContext, args);
				
			} catch(Exception e) {
				System.err.println("Exception: " + e.getMessage());
				callbackContext.error("debug line 110");
				return false;
			}	
			return true;
		} else if (action.equals("createTrip")) {
			try{
				planTrip(callbackContext, args);
				
			} catch(Exception e) {
				System.err.println("Exception: " + e.getMessage());
				callbackContext.error("debug line 120");
				return false;
			}	
			return true;
		} else if (action.equals("cancelTrip")){
			cancelTrip(callbackContext);
			return true;
		} else if (action.equals("launch")){
			startNavApp();
			return true;
		} 
		return false;
		
	}
		

    private void initInterfaces() {
        if (getClient() != null) {
            mTripManager = getClient().getTripManager();
            mTripEventManager = getClient().getTripEventManager();
        }
    }
	
	public void onDestroy() {
		registerTripListener(false);
		registerTripProgressListener(false);
		registerEtaListener(false);
		registerWaypointsArrivalListener(false);
        Log.d(TAG, "> onDestroy");
        super.onDestroy();
		if (mNavappClient != null) {
            mNavappClient.close();
            mNavappClient = null;
        }
        Log.d(TAG, "< onDestroy");
    }
	
    private void registerTripListener(boolean register) {
        if (register) {
			Log.d(TAG, "Register trip listener");
            mTripListenerRegistered = true;
            mTripManager.registerTripListener(mTripListener);
        }else{
			if(mTripListenerRegistered){
				Log.d(TAG, "Unregister trip listener");
				mTripListenerRegistered = false;
				mTripManager.unregisterTripListener(mTripListener);
			}
		}
    }
	
    private void registerTripProgressListener(boolean register) {
        if (register) {
			Log.d(TAG, "Register progress listener");
            mTripProgressListenerRegistered = true;
            mTripManager.registerTripProgressListener(mProgressListener);	
        } else {
			
			if(mTripProgressListenerRegistered){
				Log.d(TAG, "Unregister progress listener");
				mTripProgressListenerRegistered = false;
				mTripManager.unregisterTripProgressListener(mProgressListener);	
			}
		}
			
    }
	
	private void registerWaypointsArrivalListener(boolean register) {
        if (register) {
			Log.d(TAG, "Register waypoints arrival listener");
            mWaypointsArrivalListenerRegistered = true;
            mTripManager.registerWaypointsArrivalListener(mWaypointListener);
        }else{
			if(mWaypointsArrivalListenerRegistered){
				Log.d(TAG, "Unregister waypoints arrival listener");
				mWaypointsArrivalListenerRegistered = false;
				mTripManager.unregisterWaypointsArrivalListener(mWaypointListener);
			}
		}
    }
	
    private void registerEtaListener(boolean register) {
        if (register) {
            if (mTrip != null) {
				mTripWaypointsEtaChangeListener = true;
                Log.d(TAG, "Waiting ETA change notification...");
                mTripManager.registerWaypointsEtaChangeListener(mTrip, mWaypointsETAListener);
            } else {
                if (!mTripListenerRegistered) {
                    Log.d(TAG, "No Trip received - register a Trip Listener to get the active trip");
                } else {
                    Log.d(TAG, "No Trip planned");
                }
            }
        } else {
            if(mTripWaypointsEtaChangeListener){
				Log.d(TAG, "Unregister waypoint ETA listener");
				mTripManager.unregisterWaypointsEtaChangeListener(mWaypointsETAListener);
				mTripWaypointsEtaChangeListener = false;
			}
        }
    }
	
	private void startup(CallbackContext callbackContext){
		String errMsg = null;
		registerTripListener(true);			
		registerTripProgressListener(true);
		registerEtaListener(true);
		registerWaypointsArrivalListener(true);
			
		try{
			StringBuilder str = new StringBuilder();
			Thread.sleep(2000);
			
            if (mTrip != null) {
                //str.append("Planned Trip to: ").append(mTrip.getDestination().getAddress().getAddress());
				str.append(mTrip.getDestination().getAddress().getAddress());
				//Log.d(TAG, mTrip.toString());
            } else {
                str.append("No planned Trip");
				//Log.d(TAG, str.toString());
            }
			callbackContext.success(str.toString());
		} catch(Exception e) {
			errMsg = e.getMessage();
			System.err.println("Exception: " + errMsg);
			callbackContext.error(errMsg);
		}

	}
	
	private void planTrip(CallbackContext callbackContext, JSONArray args) throws JSONException{
		String errMsg = null;
		StringBuilder str = new StringBuilder();
		JSONObject arg_object = args.getJSONObject(0);
		final int numWaypoints = arg_object.getJSONObject("waypoints").length();
		try{
			Double lat = arg_object.getJSONObject("waypoints").getJSONObject("waypoint"+Integer.toString(numWaypoints)).getDouble("lat");
			Double lng = arg_object.getJSONObject("waypoints").getJSONObject("waypoint"+Integer.toString(numWaypoints)).getDouble("lng");
			final Routeable dest = getClient().makeRouteable(lat,lng);
			if (numWaypoints == 1) {

				// if no waypoints plan a normal trip
				//Log.d(TAG, "planTrip planning to lat[" + dest.getLatitude() + "] lon[" + dest.getLongitude() + "]");
				
				mTripManager.planTrip(dest, mPlanListener);
			}else{
				//Create route with multiple waypoints
				final List<Routeable> waypoints = new ArrayList<Routeable>();
				
				for (int i=1; i < numWaypoints; i++) {
					// Do something with the waypoints
					Double latw = arg_object.getJSONObject("waypoints").getJSONObject("waypoint"+Integer.toString(i)).getDouble("lat");
					Double lngw = arg_object.getJSONObject("waypoints").getJSONObject("waypoint"+Integer.toString(i)).getDouble("lng");
					//Log.d(TAG, "latw: " + latw.toString());
					waypoints.add(getClient().makeRouteable(latw,lngw));
				}
				//Log.d(TAG, "planTrip planning with waypoints to lat[" + dest.getLatitude() + "] lon[" + dest.getLongitude() + "]");
				Log.d(TAG, "DEBUG LINE 314 waypoints: " + waypoints.toString());
				//final JSONObject criteria = getCriteriaShortest();
				final JSONObject criteria = null;
				if (criteria != null) {
					mTripManager.planTrip(dest, waypoints, criteria, mPlanListener);
				} else {
					mTripManager.planTrip(dest, waypoints, mPlanListener);
					//Log.d(TAG, "Exception while preparing waypoint criteria");
				}
			}
			Thread.sleep(3000);
            if (mTrip != null) {
				registerEtaListener(true);
				str.append(mTrip.getDestination());
            } else {
                str.append("No planned Trip");
            }

			callbackContext.success(str.toString());
			
		} catch(Exception e) {
			errMsg = e.getMessage();
			System.err.println("Exception: " + errMsg);
			callbackContext.error(errMsg);
		}
	}
	
	/**
	 * Creates a JSON Object used to specify planning criteria with an optimal path
	 */
	private JSONObject getCriteriaShortest() {
		final JSONObject criteria = new JSONObject();
		try {
			criteria.put("waypoints_order", "shortest");
		} catch (JSONException e) {
			return null;
		}
		return criteria;
	}

	/**
	 * Creates a JSON Object used to specify planning criteria keeping the specified order of waypoints
	 */
	@SuppressWarnings("unused")
	private JSONObject getCriteriaSpecified() {
		final JSONObject criteria = new JSONObject();
		try {
			criteria.put("waypoints_order", "specified");
		} catch (JSONException e) {
			return null;
		}
		return criteria;
	}	
	
	
	private void cancelTrip(CallbackContext callbackContext) {
		String errMsg = null;
		try{
			Log.d(TAG, "Cancelling trip " + mTrip.toString());
			mTripManager.cancelTrip(mTrip, mCancelListener);
			callbackContext.success();
		} catch(Exception e) {
			errMsg = e.getMessage();
			System.err.println("Exception: " + errMsg);
			callbackContext.error(errMsg);
		}
	}

	private void startNavApp() {
		String errMsg = null;
		try{
			final Intent intent = new Intent(NavAppClient.ACTION_LAUNCH_NAVAPP);
			this.cordova.getActivity().startActivity(intent);
		} catch(Exception e) {
			errMsg = e.getMessage();
			System.err.println("Exception: " + errMsg);
		}	


	}
 
	private PlanListener mPlanListener = new PlanListener() {
        @Override
        public void onTripPlanResult(Trip trip, PlanResult result) {
            Log.d(TAG, "onTripPlanResult result["+result+"]");   
            if (PlanResult.PLAN_OK.equals(result)) {
				mTrip = trip;
            }else{
				Log.d(TAG, "debug line 270");
			}
        }
		
    };
	
    private PlanListener mCancelListener = new PlanListener() {
        @Override
        public void onTripPlanResult(Trip trip, PlanResult result) {
			StringBuilder str = new StringBuilder();
			str.append("{'type': 'cancel', 'result': '"+result+"'}");
			sendUpdate(str.toString());
            //Log.d(TAG, str.toString());
			
        }
    };

    private int mTripListenerLineCount = 0;
	private Trip.Listener mTripListener = new Trip.Listener() {
        @Override
        public void onTripActive(Trip trip) {
			StringBuilder str = new StringBuilder();
            Log.d(TAG, "onTripActive("+trip+")");

            if (trip != null) {
                mOldTrip = trip;
				str.append(trip.getDestination().getAddress().getAddress());
				sendUpdate(str.toString());				
            }
            mTrip = trip;

        }
    };	
	
	private static String createLatLonString(double lat, double lon) {
        StringBuilder msg = new StringBuilder();
        msg.append("lat[").append(lat).append("] lon[").append(lon).append("]");
        return msg.toString();
    }
	
	private void sendUpdate(String string){
		//b.putString( "userdata", "{ data: 'test'}" );
		b.putString( "userdata", string );
		intent.putExtras( b);
		LocalBroadcastManager.getInstance(context).sendBroadcastSync(intent);
	}	
	
    private Trip.ProgressListener mProgressListener = new Trip.ProgressListener() {
        @Override
        public void onTripArrival(Trip trip) {
            if (trip!= null) {
                Log.d(TAG, "onTripArrival lat["+trip.getDestination().getLatitude()+"] lon["+trip.getDestination().getLongitude()+"]");
				//writeToFile2("onTripArrival lat["+trip.getDestination().getLatitude()+"] lon["+trip.getDestination().getLongitude()+"]", "ProgressListener");
				sendUpdate("You have arrived at your destination");
            } else {
                Log.d(TAG, "onTripArrival(NULL)");
            }
        }

        @Override
        public void onTripProgress(Trip trip, long eta, int distanceRemaining) {
            //Log.d(TAG, "onTripProgress trip["+trip.getDestination().getLatitude()+"] lon["+trip.getDestination().getLongitude()+"] eta["+eta+"] distanceRemaining["+distanceRemaining+"]");
			StringBuilder str = new StringBuilder();
			str.append("{'eta': '"+getFormattedETA(eta, 2, distanceRemaining)+"'");
			//writeToFile2(str.toString(), "ProgressListener");
			
			if(trip != null){
				final double lat11 = trip.getDestination().getLatitude();
				str.append(",'lat': '"+String.valueOf(lat11)+"',");
				final double lng11 = trip.getDestination().getLongitude();
				str.append("'lng': '"+String.valueOf(lng11)+"'}");
			}else{
				str.append('}');
			}

			
			sendUpdate(str.toString());
        }
    };
	
    private int mWaypointListenerLinesCount = 1;
    private Trip.InfoListener mWaypointListener = new Trip.InfoListener() {
        @Override
        public void onInfo(Trip trip, List<RouteableInfo> RouteableInfoList) {
            StringBuilder str = new StringBuilder();
			
			String timeStamp = new SimpleDateFormat("yyyy/MM/dd @ HH.mm.ss").format(new Date());
			
            for (RouteableInfo info : RouteableInfoList) {
				
				String event = info.getAttributeString("event", "Event not found");
				
				str.append("{'"+event+"': {'waypoint_num':'"+mWaypointListenerLinesCount+"',");
				str.append("'ATA': '"+timeStamp+"', 'ADDRESS': ");
				String address = info.getRouteable().getAddress().getAddress().toString();
				
				String address2 = address.replace('\"','\'');
				str.append(address2);
                str.append("}}\n");
				++mWaypointListenerLinesCount;
            }
			
			//{'waypoint_arrival': {'waypoint_num': '0','ATA': '2017/06/14 @ 15.17.31','ADDRESS': {'country_code': 'ZAF','street': 'Unnamed road','city': 'Cape Town','country': 'South Africa'}}}}
			
			//writeToFile2(str.toString(), "WaypointListener");
			//Log.d(TAG, str.toString());
			sendUpdate(str.toString());
        }
		
        @Override
        public void onError(RequestError error) {
			Log.d(TAG, "Error occurred while registering: " + error);
        }
    };	
	
    private static String getFormattedETA(long etaInSecs, int offset, int distanceRemaining) {
        Calendar utcCalendar = Calendar.getInstance(TimeZone.getTimeZone("Africa/Johannesburg"));
        final long utcInMs = etaInSecs * 1000;
        utcCalendar.setTimeInMillis(utcInMs);
        final int year  = utcCalendar.get(Calendar.YEAR);
        final int month = (utcCalendar.get(Calendar.MONTH) + 1);    // Calendar.MONTH returns 0 for January
        final int day   = utcCalendar.get(Calendar.DAY_OF_MONTH);
        final int hours = utcCalendar.get(Calendar.HOUR_OF_DAY);
        final int mins  = utcCalendar.get(Calendar.MINUTE);
        final int secs  = utcCalendar.get(Calendar.SECOND);
        final StringBuilder dateString = new StringBuilder();
        dateString.append(String.format("%d/%02d/%02d @ %02d:%02d:%02d", year, month, day, hours, mins, secs));
        if (offset != 0) {
            boolean isEarlier = false;
            final long offsetInMs;
            if (offset < 0) {
                isEarlier = true;
                offsetInMs = - (offset * 1000);
            } else {
                offsetInMs = offset * 1000;
            }
            utcCalendar.setTimeInMillis(offsetInMs);
            if (isEarlier) {
                dateString.append(String.format("", utcCalendar.get(Calendar.HOUR_OF_DAY), utcCalendar.get(Calendar.MINUTE)));
            } else {
                dateString.append(String.format("", utcCalendar.get(Calendar.HOUR_OF_DAY), utcCalendar.get(Calendar.MINUTE)));
            }
        }
        if (distanceRemaining != -1) {
            //dateString.append(String.format(", Dist remaining: %d mts", distanceRemaining));
			
			float dist_kms = Float.valueOf(distanceRemaining) / 1000;
			dateString.append(String.format(", Dist remaining: %.2f kms", dist_kms));
        }
        return dateString.toString();
    }	

    private Trip.InfoListener mWaypointsETAListener = new Trip.InfoListener() {
        @Override
        public void onInfo(Trip trip, List<RouteableInfo> waypointsEtaInfoList) {
            // List the ETA for the first 3 waypoints
            final StringBuilder builder = new StringBuilder();
            for (int waypointIndex = 0; waypointIndex < 3 && waypointIndex < waypointsEtaInfoList.size(); waypointIndex++) {
                final RouteableInfo routeableInfo = waypointsEtaInfoList.get(waypointIndex);
                final Routeable waypoint = routeableInfo.getRouteable();
				//if(waypointIndex == 0){
				//	Log.d(TAG, "Debug line 550 routeableInfo");
				//	Log.d(TAG, waypoint.getLongitude());
				//}
                final long eta = routeableInfo.getAttributeLong("eta", -1);
                final int offset = routeableInfo.getAttributeInt("offset", -1);
				//Log.d(TAG, routeableInfo.toString());
				double lat111 = routeableInfo.getRouteable().getLatitude();
				double lng111 = routeableInfo.getRouteable().getLongitude();
				String address1 = routeableInfo.getRouteable().getAddress().getAddress().toString();
                builder.append("'").append(waypointIndex).append("':{'ETA':");
                
                if (eta > 0) {
					builder.append("'").append(getFormattedETA(eta, offset, -1)).append("','address':'"+address1+"','lat':'"+String.valueOf(lat111)+"','lng':'"+String.valueOf(lng111)+"'}");
                } else {
                    // Waypoint has been passed or ETA cannot be determined
                    // (e.g. AtoB route, not an active route and so on)
					builder.append("'Not available'}");
                }
				if(waypointIndex < waypointsEtaInfoList.size() - 1){
					builder.append(",");
				}
            }
			
			StringBuilder str = new StringBuilder();
			str.append("{'waypoints': {"+builder.toString()+"}}");
			//Log.d(TAG, "Debug line 573 waypointlistener return");
			//Log.d(TAG, str.toString());
			//writeToFile2(str.toString(), "WaypointsETAListener");
			sendUpdate(str.toString());
        }

        @Override
        public void onError(RequestError error) {
			Log.d(TAG, "Failed registering ETA error[" + error + "]");
            sendUpdate("Failed registering ETA error[" + error + "]");
        }
		
    };
	
	public void writeToFile2(String data, String module){
		String pathname = Environment.getExternalStorageDirectory() + File.separator  + "myFolder";
		String filename = pathname + File.separator + "nav.log";
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss").format(new Date());
		
		File folder = new File(pathname);
		if(!folder.exists()){
			folder.mkdirs();
		}
		//[Mon Jun 12 11:58:24.870860 2017]
		String logdata = "[" + timeStamp + "][" + module + "]: " + data;
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
			out.println(logdata);
			out.close();
		} catch (IOException e) {
			//exception handling left as an exercise for the reader
			Log.e(TAG, "file error ", e);
		}
		
	}
	
	public void writeToFileDebug(String data, String module){
		String pathname = Environment.getExternalStorageDirectory() + File.separator  + "myFolder";
		String filename = pathname + File.separator + "debug.log";
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss").format(new Date());
		
		File folder = new File(pathname);
		if(!folder.exists()){
			folder.mkdirs();
		}
		//[Mon Jun 12 11:58:24.870860 2017]
		String logdata = "[" + timeStamp + "][" + module + "]: " + data;
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
			out.println(logdata);
			out.close();
		} catch (IOException e) {
			//exception handling left as an exercise for the reader
			Log.e(TAG, "file error ", e);
		}
		
	}	
	
	
}
